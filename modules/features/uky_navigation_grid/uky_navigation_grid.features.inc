<?php
/**
 * @file
 * uky_navigation_grid.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function uky_navigation_grid_image_default_styles() {
  $styles = array();

  // Exported image style: navigation_grid_large.
  $styles['navigation_grid_large'] = array(
    'label' => 'navigation grid large',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 720,
          'height' => 540,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uky_navigation_grid_node_info() {
  $items = array(
    'navigation_grid_tile' => array(
      'name' => t('Navigation Grid Tile'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uky_navigation_grid_paragraphs_info() {
  $items = array(
    'navigation_grid' => array(
      'name' => 'Navigation Grid',
      'bundle' => 'navigation_grid',
      'locked' => '1',
    ),
  );
  return $items;
}
