<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class=" uky_navigation-grid container-fluid <?php print $classes; ?>" style="background-color:  <?php print $content['field_background_color']['#items'][0]['rgb']; ?>;"<?php print $attributes; ?>>
    <div class="uky_navigation-grid__container container">



        <?php foreach ($content['field_navigation_grid_tiles']['#items'] as $tile){
            $tileNode = node_view($tile['entity'], 'navigation_grid_tile');
            $tileNode['#node']->columns = $content['field_navigation_grid_columns']['#items'][0]['value'];
            print render($tileNode);

        } ?>
        <?php //print render($content['field_navigation_grid_tiles']); ?>
    </div>
</div>
